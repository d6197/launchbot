require("dotenv-safe").config();
const api = require("./api");
var events = require("events");

const WebSocket = require("ws");
const ws = new WebSocket("wss://stream.binance.com:9443/ws/!bookTicker");

this.isBought = false;
let count = 0;
let decreasingPercentage = 0.0;

var eventEmitter = new events.EventEmitter();

var eventHandler = async function (obj, count, decreasingPercentage) {
  try {
    let buyQuantity = 0;
    const SYMBOL = process.env.SYMBOL;
    const inicialBuyQty = parseFloat(process.env.INITIAL_BUY_QUANTITY); //500
    const stopBuyQty = parseFloat(process.env.STOP_BUY_QUANTITY);
    count = count + 1;
    if (count == 1) {
      buyQuantity = inicialBuyQty;
      console.log(`count = ${count} || buyQuantity = ${buyQuantity}`);
      //console.log(obj.s);
      const order = await api.newOrder(SYMBOL, buyQuantity, 0);
      console.log(order);
      if (order.status === "FILLED") {
        process.exit();
      }
    } else {
      buyQuantity = inicialBuyQty - inicialBuyQty * decreasingPercentage;
      console.log(`count = ${count} || buyQuantity = ${buyQuantity}`);
      if (buyQuantity <= stopBuyQty) {
        process.exit();
      }
      const order = await api.newOrder(SYMBOL, buyQuantity.toFixed(6), 0);
      console.log(order);
      if (order.status === "FILLED") {
        process.exit();
      }
    }
  } catch (err) {
    console.error(err.response.data.msg);
    if (err.response.data.code !== -2010) {
      process.exit();
    }
    decreasingPercentage = decreasingPercentage + 0.02;
    eventEmitter.emit("callBinance", obj, count, decreasingPercentage);
  }
};

eventEmitter.on("callBinance", eventHandler);

ws.on("error", (err) => {
  console.log("WS Error");
  console.error(err);
});

ws.onmessage = async (event) => {
  try {
    const SYMBOL = process.env.SYMBOL;
    const obj = JSON.parse(event.data);
    this.obj = obj;
    if (!this.isBought) {
      if (obj.s === SYMBOL) {
        this.isBought = true;
        eventEmitter.emit("callBinance", obj, count, decreasingPercentage);
      }
    }
  } catch (err) {
    console.error(err);
  }
};

ws.onclose = () => {
  console.log("closed");
};
