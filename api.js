const axios = require("axios");
const queryString = require("querystring");

const crypto = require("crypto");

/*
async function newQuoteOrder(symbol, quoteOrderQty) {
  const data = { symbol, side: "BUY", type: "MARKET", quoteOrderQty };
  return privateCall("/v3/order", data, "POST");
}*/

async function newOrder(
  symbol,
  quantity,
  price,
  side = "BUY",
  type = "MARKET"
) {
  const data = { symbol, side, type, quantity };

  if (price) data.price = parseInt(price);
  if (type === "LIMIT") data.timeInForce = "GTC";

  return privateCall("/v3/order", data, "POST");
}

async function privateCall(path, data = {}, method = "GET") {
  let apiKey = process.env.API_KEY_DEV,
    apiSecret = process.env.SECRET_KEY_DEV,
    apiUrl = process.env.API_URL_DEV;

  if (process.env.ENV_CONF === "PRD") {
    apiKey = process.env.API_KEY;
    apiSecret = process.env.SECRET_KEY;
    apiUrl = process.env.API_URL;
  }

  if (!apiKey || !apiSecret)
    throw new Error("Preencha corretamente sua API KEY e SECRET KEY");

  const timestamp = Date.now();
  const recvWindow = 60000; //máximo permitido, default 5000

  const signature = crypto
    .createHmac("sha256", apiSecret)
    .update(`${queryString.stringify({ ...data, timestamp, recvWindow })}`)
    .digest("hex");

  const newData = { ...data, timestamp, recvWindow, signature };
  const qs = `?${queryString.stringify(newData)}`;

  //try {
  const result = await axios({
    method,
    url: `${apiUrl}${path}${qs}`,
    headers: { "X-MBX-APIKEY": apiKey },
  });
  return result.data;
  //} catch (err) {
  //  console.error(err.response.data);
  //process.exit();
  //}
}

module.exports = { newOrder };
